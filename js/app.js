var Vue = require('vue');
Vue.use(require('vue-resource'));

new Vue({
  el: '#menuCtrl',
  data: {
    service: { name: '', class: '', activated: '', type: '', value: '', url: '' },
    services: [],
    root: 'https://socialcredit.marcrijken.org'
  },
  ready: function(){
    this.fetchServices();
  },
  methods: {
    fetchServices: function(){

      // // @TODO: Hardcoded services vervangen vanuit API
      // this.$http.get('api/services').success(function(services) {
      //   this.$set('services', services);
      // }).error(function(error) {
      //   console.log(error);
      // });

      // Of POST voorbeeld
      // this.$http.post('api/services', this.service).success(function(response) {
      //   this.services.push(this.service);
      // }).error(function(error) {
      //   console.log(error);
      // });

      var services = [
        {
          name: 'Email',
          class: 'fa-envelope',
          activated: true,
          type: 'input',
          url: '/api/email',
          value: 'max.schulkens@sns.nl'
        },
        {
          name: 'Facebook',
          class: 'fa-facebook',
          activated: false,
          type: 'api',
          url: '/api/facebook'
        },
        {
          name: 'Twitter',
          class: 'fa-twitter',
          activated: false,
          type: 'api',
          url: '/api/twitter'
        },
        {
          name: 'Linkedin',
          class: 'fa-linkedin',
          activated: false,
          type: 'api',
          url: '/api/linkedin'
        },
        {
          name: 'Airbnb',
          class: 'fa-home',
          activated: false,
          type: 'input',
          url: '/api/airbnb'
        },
        {
          name: 'Peerby',
          class: 'fa-comment-o',
          activated: false,
          type: 'input',
          url: '/api/peerby'
        },
        {
          name: 'Marktplaats',
          class: 'fa-newspaper-o',
          activated: false,
          type: 'input',
          url: '/api/marktplaats'
        }
      ];
      for (var i = 0; i<services.length; i++) {
        services[i].showInput = false;
        services[i].dirty = false;
        services[i].loading = false;
      }
      this.$set('services', services);
    },

    toggleInput: function(index){
      var service = this.services[index];
      switch (service.type) {
        case 'input':
          service.showInput = !service.showInput;
          break;
        case 'api':
          console.log('api');
          break;
      }
    },

    toggleButtons: function(index){
      var service = this.services[index];
      service.dirty = true;
    },

    saveInput: function(index, url){
      var service = this.services[index];
      service.loading = true;

      // @TODO: api call maken
      // this.$http.post(this.root + url, service).success(function(resp) {
      //   service.dirty = false;
      //   service.loading = false;
      //   var notification = new NotificationFx({
      //   message : '<p>' + {resp.message} + '</p>',
      //   layout : 'growl',
      //   effect : 'scale',
      //   type : 'notice', // notice, warning, error or success
      //   onClose : function() {
      //     console.log('dismissed notification');
      //   }
      // });
      // notification.show();
      // }).error(function(error) {
      // var notification = new NotificationFx({
      //   message : '<p>' + '{service.name} succesvol gelinkt! Maak je profiel compleet en link er nog meer.' + '</p>',
      //   layout : 'growl',
      //   effect : 'scale',
      //   type : 'error', // notice, warning, error or success
      //   onClose : function() {
      //     console.log('dismissed notification');
      //   }
      // });
      // notification.show();
      //   service.loading = false;
      //   console.log(error);
      // });

      // demo purposes
      setTimeout(function(){
        var notification = new NotificationFx({
          message : '<p>' + '{service.name} succesvol gelinkt! Maak je profiel compleet en link er nog meer.' + '</p>',
          layout : 'growl',
          effect : 'scale',
          type : 'warning', // notice, warning, error or success
          onClose : function() {
            console.log('dismissed notification');
          }
        });
        notification.show();
        service.dirty = false;
        service.loading = false;
      }, 1500)
    }
  }
})

new Vue({
  el: '#main',
  data: {
    user: {name: '', mail: ''}
  },
  ready: function(){
    this.fetchData();
  },
  methods: {
    fetchData: function(){
      // Call maken om data binnen te halen
      var user = {
        name: 'Max',
        mail: 'max.schulkens@sns.nl'
      }
      this.$set('user', user);
    }
  }
});
